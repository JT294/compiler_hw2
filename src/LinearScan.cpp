#include <algorithm>
#include "LinearScan.h"
#include "MachineCode.h"
#include "LiveVariableAnalysis.h"

LinearScan::LinearScan(MachineUnit *unit)
{
    this->unit = unit;
   // for (int i = 4; i < 11; i++)
    for (int i=10; i>3; i--)
        regs.push_back(i);
}

void LinearScan::allocateRegisters()
{
    for (auto &f : unit->getFuncs())
    {
        func = f;
        bool success;
        success = false;
        while (!success)        // repeat until all vregs can be mapped
        {
            computeLiveIntervals();
            success = linearScanRegisterAllocation();
            if (success)        // all vregs can be mapped to real regs
                modifyCode();
            else                // spill vregs that can't be mapped to real regs
                genSpillCode();
        }
    }
}

void LinearScan::makeDuChains()
{
    LiveVariableAnalysis lva;
    lva.pass(func);
    du_chains.clear();
    int i = 0;
    std::map<MachineOperand, std::set<MachineOperand *>> liveVar;
    for (auto &bb : func->getBlocks())
    {
        liveVar.clear();
        for (auto &t : bb->getLiveOut())
            liveVar[*t].insert(t);
        int no;
        no = i = bb->getInsts().size() + i;
        for (auto inst = bb->getInsts().rbegin(); inst != bb->getInsts().rend(); inst++)
        {
            (*inst)->setNo(no--);
            for (auto &def : (*inst)->getDef())
            {
                if (def->isVReg())
                {
                    auto &uses = liveVar[*def];
                    du_chains[def].insert(uses.begin(), uses.end());
                    auto &kill = lva.getAllUses()[*def];
                    std::set<MachineOperand *> res;
                    set_difference(uses.begin(), uses.end(), kill.begin(), kill.end(), inserter(res, res.end()));
                    liveVar[*def] = res;
                }
            }
            for (auto &use : (*inst)->getUse())
            {
                if (use->isVReg())
                    liveVar[*use].insert(use);
            }
        }
    }
}
 
void LinearScan::computeLiveIntervals()
{
    makeDuChains();
    intervals.clear();
    for (auto &du_chain : du_chains)
    {
        int t = -1;
        for (auto &use : du_chain.second)
            t = std::max(t, use->getParent()->getNo());
        Interval *interval = new Interval({du_chain.first->getParent()->getNo(), t, false, 0, 0, {du_chain.first}, du_chain.second});
        intervals.push_back(interval);
    }
    bool change;
    change = true;
    while (change)
    {
        change = false;
        std::vector<Interval *> t(intervals.begin(), intervals.end());
        for (size_t i = 0; i < t.size(); i++)
            for (size_t j = i + 1; j < t.size(); j++)
            {
                Interval *w1 = t[i];
                Interval *w2 = t[j];
                if (**w1->defs.begin() == **w2->defs.begin())
                {
                    std::set<MachineOperand *> temp;
                    set_intersection(w1->uses.begin(), w1->uses.end(), w2->uses.begin(), w2->uses.end(), inserter(temp, temp.end()));
                    if (!temp.empty())
                    {
                        change = true;
                        w1->defs.insert(w2->defs.begin(), w2->defs.end());
                        w1->uses.insert(w2->uses.begin(), w2->uses.end());
                        w1->start = std::min(w1->start, w2->start);
                        w1->end = std::max(w1->end, w2->end);
                        auto it = std::find(intervals.begin(), intervals.end(), w2);
                        if (it != intervals.end())
                            intervals.erase(it);
                    }
                }
            }
    }
    sort(intervals.begin(), intervals.end(), compareStart);
}

bool LinearScan::linearScanRegisterAllocation()
{
    // Todo
    active.clear();
    bool success = true;
    for (auto &interval : intervals)
    {
        expireOldIntervals(interval);
        if ( active.size()== 7 )  //what is the number of avilable register
        {
            spillAtInterval(interval);
            success = false;
        }
        else
        {
            //how to do register[i] ← a register removed from pool of free registers
            //add i to active, sorted by increasing end point
            interval->rreg = regs.back();
            regs.pop_back();
            if (active.size()==0)
            {
                active.insert(active.begin(),interval);
            }
            else
            {
                for (auto &j : active)
                {
                    if(interval->end<j->end)
                    {
                        auto it = std::find(active.begin(),active.end(),j);  //is it right?
                        int dist = std::distance(active.begin(),it);
                        active.insert(active.begin()+dist,interval);  //do i need to remove interval from intervals
                        break;
                    }
                }
            }
            
        }
    }
    if(!success)
        return false;
    return true;
}

void LinearScan::modifyCode()
{
    for (auto &interval : intervals)
    {
        func->addSavedRegs(interval->rreg);
        for (auto def : interval->defs)
            def->setReg(interval->rreg);
        for (auto use : interval->uses)
            use->setReg(interval->rreg);
    }
}

void LinearScan::genSpillCode()
{
    for(auto &interval:intervals)
    {
        if(!interval->spill)
            continue;
        // TODO
        /* HINT:
         * The vreg should be spilled to memory.
         * 1. insert ldr inst before the use of vreg
         * 2. insert str inst after the def of vreg
         */ 

        
        for(auto &i :interval->uses)
        {
            auto inst_no = i->getParent()->getNo();
            auto block_no = i->getParent()->getParent()->getNo();
            for(auto &mb : func->getBlocks())
            {
                if(mb->getNo()==block_no)
                {
                    for(auto &min : mb->getInsts())
                    {
                        if(inst_no==min->getNo())
                        {
                            MachineInstruction* cur_inst = nullptr;
                            MachineOperand* src1 = new MachineOperand(MachineOperand::REG,11);
                            MachineOperand* src2 = new MachineOperand(MachineOperand::IMM,interval->disp);
                            cur_inst = new LoadMInstruction(mb,i,src1,src2);
                            mb->InsertBefore(cur_inst,inst_no);
                            makeDuChains();
                        }
                    }
                }
            }
        }
        for(auto &i : interval->defs)
        {
            auto inst_no = i->getParent()->getNo();
            auto block_no = i->getParent()->getParent()->getNo();
            for(auto &mb : func->getBlocks())
            {
                if(mb->getNo()==block_no)
                {
                    for(auto &min : mb->getInsts())
                    {
                        if(inst_no==min->getNo())
                        {
                            MachineInstruction* cur_inst = nullptr;
                            MachineOperand* src1 = new MachineOperand(MachineOperand::REG,11);
                            MachineOperand* src2 = new MachineOperand(MachineOperand::IMM,interval->disp);
                            cur_inst = new LoadMInstruction(mb,i,src1,src2);
                            mb->InsertBack(cur_inst,inst_no);
                            makeDuChains();
                        }
                    }
                }
            }
        }
        
    }
    
}

void LinearScan::expireOldIntervals(Interval *interval)
{
    // Todo
    for (auto &j :active)
    {
        if(j->end>=interval->start)
            return;
        auto it = std::find(active.begin(),active.end(),j);
        active.erase(it);
        regs.push_back(j->rreg);
    }
}

void LinearScan::spillAtInterval(Interval *interval)
{
    // Todo
    auto spill = active.back();
    if(spill->end>interval->end)
    {
        interval->rreg = spill->rreg;
        spill->spill=true;
        spill->disp = func->AllocSpace(4);
        auto it = std::find(active.begin(),active.end(),spill);
        active.erase(it);
        for (auto &j : active)
        {
            if(interval->end<j->end)
            {
                auto it = std::find(active.begin(),active.end(),j);  //is it right?
                int dist = std::distance(active.begin(),it);
                active.insert(active.begin()+dist,interval);  //do i need to remove interval from intervals
                break;
            }
        }
    }
    else
    {
        interval->spill=true;
        interval->disp = func->AllocSpace(4);
    }
    auto its = std::find(intervals.begin(),intervals.end(),interval);
    intervals.erase(its);
}

bool LinearScan::compareStart(Interval *a, Interval *b)
{
    return a->start < b->start;
}